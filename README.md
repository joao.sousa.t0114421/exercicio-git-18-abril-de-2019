Exercicio GIT
* I - Crie um repositório para cada ficha de exercícios das aulas de PHP e coloque cada um dos exercícios numa
nova branch. Na branch master deve colocar o enunciado da ficha. 
*  II - Aceda ao repositório https://gitlab.com/atec-pt/tpsip1018/exercicio-git e execute as ações lá descritas.
*  III - Crie um site em PHP que:
1. Contenha a class Git\Repo com os atributos nome, url e descrição.
2. Na página inicial apresente uma lista de servidores GIT com os campos nome e url e:
a. Ao clicar no url, este deve direcionar o utilizador para o endereço do servidor.
b. Ao clicar no nome, deve ser direcionado para uma página onde aparece o titulo e um
pequeno resumo sobre o mesmo.
3. Garanta que a pasta vendor não é enviada para o servidor.
NOTA: Todos os pontos do grupo III devem refletir um commit num repositório criado por si.